# Daily Cat [beta]

A Discord.js bot to send daily cat photos.

Invite the bot [here](https://discord.com/api/oauth2/authorize?client_id=965370985417171005&permissions=51200&scope=bot%20applications.commands).

Set up a channel for cats to be sent to every day, by doing `/setchannel`. You must have the ability to manage channels in order to use this command. Then, every day at 11 PM BST, a cat will be sent there :)
