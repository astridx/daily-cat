// Gives a role access to a slash command
async function addPermission(commandName, role, client) {
  if (!client.application?.owner) await client.application?.fetch();
  let commands = await client.guilds.cache
    .get(process.env.GUILD_ID)
    ?.commands.fetch();
  commands.map(async (cmd) => {
    if (cmd.name === commandName) {
      const permissions = [
        {
          id: role,
          type: "ROLE",
          permission: true,
        },
      ];

      try {
        await cmd.permissions.add({ permissions });
        console.log(`Added permission for ${cmd.name}`);
      } catch (err) {
        console.log(err);
      }
    }
  });
}

// Gives a specific user access to a slash command
async function addPermissionToUser(commandName, user, client) {
  if (!client.application?.owner) await client.application?.fetch();
  let commands = await client.guilds.cache
    .get(process.env.GUILD_ID)
    ?.commands.fetch();
  commands.map(async (cmd) => {
    if (cmd.name === commandName) {
      const permissions = [
        {
          id: user,
          type: "USER",
          permission: true,
        },
      ];

      try {
        await cmd.permissions.add({ permissions });
        console.log(`Added permission for ${cmd.name}`);
      } catch (err) {
        console.log(err);
      }
    }
  });
}

// Wipes all permissions for the specified slash command
async function wipePermissions(commandName, client) {
  if (!client.application?.owner) await client.application?.fetch();
  let commands = await client.guilds.cache
    .get(process.env.GUILD_ID)
    ?.commands.fetch();
  commands.map(async (cmd) => {
    if (cmd.name === commandName) {
      try {
        await cmd.permissions.set({ command: cmd.id, permissions: [] });
        console.log(`Wiped permissions for ${cmd.name}`);
      } catch (err) {
        console.log(err);
      }
    }
  });
}

/**
 * Convert string to argument type
 *
 * @export
 * @return {number} The argument type
 * @param {string} _type The string to convert into an argument type
 */
function getArgType(_type) {
  let type = _type.toUpperCase();
  type = type.split(" ").join("_");

  let commandTypes = {
    SUB_COMMAND: 1,
    SUB_COMMAND_GROUP: 2,
    STRING: 3,
    INTEGER: 4,
    BOOLEAN: 5,
    USER: 6,
    CHANNEL: 7,
    ROLE: 8,
    MENTIONABLE: 9,
  };

  if (commandTypes[type]) return commandTypes[type];
}


module.exports = {
  wipePermissions,
  addPermission,
  addPermissionToUser,
  getArgType,
};
