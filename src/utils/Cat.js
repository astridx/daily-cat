const reddit = require("reddit.images");
const Chance = require("chance").Chance;
let chance = new Chance();

// list of subreddits to get cats from
const subreddits = [
  "catpics",
  "cats",
  "CatsStandingUp",
]

// Function to get a cat
async function getCat() {

  logger.debug({
    title: "Cat.js",
    messages: [
      `Getting cat image`
    ]
  })

  // Yet another function, except this actually gets the image
  // This function is recalled in the case of there being no image
  async function generate() {
    // get a random subreddit
    const subreddit = chance.pickone(subreddits);

    // fetch a random post
    const catPost = await reddit.FetchSubredditPost({
      subreddit
    })

    // Get the image
    let image = catPost?.image;

    // sometimes the post has no images attached (wtf???)
    if(!image) {
      logger.error({
        title: "Cat.js",
        messages: [
          `No image!!`
        ]
      })

      // we re-generate
      return generate();
    }

    // if the post has multiple, we grab the first one
    if(typeof catPost.image == "object") image = catPost.image[0];

    // sometimes an image is not returned, in that case we just regenerate
    if(image === "https://i.imgur.com/undefined.png") return generate()

    return image;
  }

  return generate();
}

module.exports = getCat;