const cron = require('node-cron');
const { EmbedBuilder } = require("discord.js");

// then we will be able to loop through all servers, get their cat channel, & boom

async function main() {
  logger.debug({
    title: `SendCat`,
    messages: [
      `Main function called`
    ]
  })

  // Get all the servers within the database
  const _servers = DB.get("Servers");
  const allServers = await _servers.find({});

  // This function is called per server to send a cat photo  there
  async function sendCat(server = {
    id: null,
    catChannel: null
  }) {
    // "server" has their cat channel, as well as the ID of the server.
    if(server.catChannel == null) return;

    let guild;
    let channel;

    // Try to fetch the guild as well as the channel.
    try {
      guild = await client.guilds.fetch(server.id);
      channel = await guild.channels.fetch(server.catChannel);
    } catch(err) {
      // catch any errors
      logger.error({
        title: `SendCat`,
        messages: [`Could not find Guild or Channel (Guild ID: ${guild})`]
      })
      return;
    }

    // Generate a cat post
    let catPost = await CatImage();

    // Then create & send the embed
    const imageEmbed = new EmbedBuilder().setColor("Random").setImage(catPost).setFooter({
      text: `${pkg.name} - v${pkg.version}`
    })

    channel.send({
      embeds: [imageEmbed]
    })

    let catObj = {
      url: catPost,
      // this will be used soon 👀
      reactions: {
        love: 0,
        cute: 0,
        like: 0
      }
    }

    server.cats.push(catObj)

    _servers.update({
      id: server.id
    },{$set: server})

  }

  // Gets all the servers, then passes their server data into the sendCat function
  allServers.map((server) => sendCat(server))
}

// Calls this function every day at 12 AM BST (or, the timezone the bot is running in.)
cron.schedule('0 0 * * *',  main);
