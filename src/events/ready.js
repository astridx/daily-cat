const { Handler } = require("../CommandHandler");

let cmdHandler = new Handler();
// Pass in "true" if you haven't updated any commands (name, description, arguments)
// This prevents rate limits from Discord if you're constantly restarting the bot.
cmdHandler.setup();

module.exports = {
  name: "ready",
  async execute(client) {

    logger.debug({
      title: `Ready`,
      messages: [`Bot is now online!`],
    });

    require("../SendCat")
    global.client = client;

    await client.guilds.fetch()

    logger.debug({
      title: "Ready",
      messages: [`Sending cats to ${client.guilds.cache.size} servers!`]
    })
  },
};

module.exports.getCmdHandler = function () {
  return cmdHandler;
};
