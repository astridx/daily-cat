const { getCmdHandler } = require("./ready");
const { existsSync } = require("fs");
const { join } = require("path");

let cmdHandler = getCmdHandler();

// Interaction create handler, this doesn't really need to be read / edited

module.exports = {
  name: "interactionCreate",
  async execute(interaction, client) {

    const {guild} = interaction;

    if (
      !interaction.isContextMenuCommand() &&
      !interaction.isCommand() &&
      !interaction.isSelectMenu() &&
      !interaction.isButton()
    )
      return;

    const _servers = DB.get("Servers");
    interaction.currentServer = await _servers.findOne({
      id: guild.id
    });

    if(!interaction.currentServer) {
      logger.debug({
        title: `Interaction Create`,
        messages: [`No guild was found in the database, so I created one ${guild.name} - ${guild.id}`],
      });

      await _servers.insert({
        id: guild.id,
        joinedAt: Date.now(),
        cats: []
      })

      interaction.currentServer = await _servers.findOne({
        id: guild.id
      });
    }

    if (interaction.isModalSubmit()) {
      if (existsSync(join(__dirname, `modal`, `${interaction.customId}.js`)))
        require(join(__dirname, `modal`, `${interaction.customId}.js`)).execute(
          client,
          interaction
        );
      return;
    }

    if (interaction.isButton()) {
      if (existsSync(join(__dirname, `button`, `${interaction.customId}.js`)))
        await require(join(
          __dirname,
          `button`,
          `${interaction.customId}.js`
        )).execute(client, interaction);
      return;
    }

    // Essentially this is the message create event
    cmdHandler
      .getCommand(interaction.commandName)
      .then((cmd) => {
        try {
          const cmdfile = require(cmd["file"]);
          cmdfile.execute(client, interaction);
        } catch (err) {
          console.log(err);
          console.log(`Error executing a command`);
        }
      })
      .catch((err) => {
        console.log(err);
      });
  },
};
