// On guild join, we add the server to the database

module.exports = {
  name: "guildCreate",
  async execute(guild, client) {
    const servers = DB.get("Servers");

    await servers.insert({
      id: guild.id,
      joinedAt: Date.now(),
      cats: []
    })

    logger.debug({
      title: `Guild Join`,
      messages: [`${guild.name} - ${guild.id}`],
    });

  }
}