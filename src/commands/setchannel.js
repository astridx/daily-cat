const { getArgType } = require("../utils/other-util");
const { PermissionsBitField } = require("discord.js");

module.exports = {
  name: "setchannel",
  description: "Sets the channel to send cat photos to",
  options: [
    {
      name: "channel",
      description: "The channel to send cat photos in",
      type: getArgType("channel"),
      required: true,
    },
  ],
  async execute(client, interaction) {
    const _servers = DB.get("Servers");

    const { member } = interaction;

    if (!member.permissions.has(PermissionsBitField.Flags.ManageChannels))
      return interaction.reply({
        content: `no perms, nerd.`,
        ephemeral: true,
      });

    // Defer the reply
    await interaction.deferReply();

    // Get the channel to set it to
    const channel = interaction.options.getChannel("channel");

    // Create a variable & grab the current server from the interaction
    const server = interaction.currentServer;

    // If it doesn't exist (this shouldn't happen), then we return
    if (!server) {
      interaction.editReply({
        content: `Hmm, this server is not saved in our database. Try to re-invite the bot.`,
      });
      return;
    }

    // If it does exist however, it will get here
    // We then set their catChannel to the channel selected
    server.catChannel = channel.id;

    // Update the entry within the database
    await _servers.update(
      {
        id: server.id,
      },
      { $set: server }
    );

    // Then just edit the reply
    interaction.editReply({
      content: `Cat photos will now be sent in <#${channel.id}> every day at 00:00 BST`,
    });
  },
};
