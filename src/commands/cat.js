const { EmbedBuilder } = require("discord.js");

module.exports = {
  name: "cat",
  description: "Sends a cat photo",
  async execute(client, interaction) {
    // Defer the reply
    await interaction.deferReply();

    // Get a cat image using the utility, this returns its' image url
    const catPost = await CatImage();

    // Create an embed & set the image to the cat post
    const imageEmbed = new EmbedBuilder().setColor("Random").setImage(catPost).setFooter({
      text: `${pkg.name} - v${pkg.version}`
    })

    // then edit the reply with the embed
    await interaction.editReply({
      embeds: [imageEmbed]
    });
  },
};
